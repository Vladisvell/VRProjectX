using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStratagem
{
    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemController.StratagemControls>();
    }

    public GameObject projectilePrefab {get; set;}
    public static string stratagemName { get; set;}    
}
