using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static StratagemController;

public class Illumination : MonoBehaviour, IStratagem
{
    public GameObject projectilePrefab { get => EMSPrefab; set => EMSPrefab = value; }

    private GameObject EMSPrefab;
    public static string stratname = "Lights";
    public static string stratagemName { get => stratname; set => stratname = value; }
    bool spawned = false;

    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemControls> { StratagemControls.Right, StratagemControls.Right, StratagemControls.Left, StratagemControls.Left };
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnIllumination()
    {
        var obj = Instantiate<GameObject>(EMSPrefab, transform.transform.position + Vector3.up * 170, EMSPrefab.transform.rotation);
    }

    public void DestroyMe()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if(spawned)
                return;
            Invoke("SpawnIllumination", 5);
            Invoke("DestroyMe", 5);
            spawned = true;
        }
    }
}
