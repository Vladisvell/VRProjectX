using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static StratagemController;

public class Gatling : MonoBehaviour, IStratagem
{
    [SerializeField] //
    private GameObject bulletPrefab;
    private int bulletCount = 100;
    private float spreadRadius = 10;
    public GameObject projectilePrefab { get => bulletPrefab; set => bulletPrefab = value; }
    public static string stratname = "Gatling";
    public static string stratagemName { get => stratname; set => stratname = value; }

    bool summoned = false;

    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemControls> {StratagemControls.Right, StratagemControls.Down, StratagemControls.Left, StratagemControls.Up, StratagemControls.Up };
    }


    private IEnumerator SpawnBarrage()
    {
        yield return new WaitForSeconds(5);
        Vector3 pos = transform.position + Vector3.up * 170;
        for (int i = 0; i < bulletCount; i++)
        {
            Vector3 bulletPos = pos;
            float xComp = Mathf.Sin(Random.value * Mathf.PI * 2) * spreadRadius;
            float yComp = i;
            float zComp = Mathf.Cos(Random.value * Mathf.PI * 2) * spreadRadius;
            bulletPos.x += xComp;
            bulletPos.y += yComp;
            bulletPos.z += zComp;
            yield return new WaitForSeconds(0.2f);
            Instantiate(bulletPrefab, bulletPos, Quaternion.identity);            
        }
        Invoke("SelfDestroy", 5);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if (!summoned)
            {
                StartCoroutine(SpawnBarrage());                
                summoned = true;
            }
        }
    }

    private void SelfDestroy()
    {
        Destroy(this.gameObject);
    }
}
