using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static StratagemController;

public class GasStrike : MonoBehaviour, IStratagem
{
    public GameObject projectilePrefab { get => EMSPrefab; set => EMSPrefab = value; }

    public GameObject EMSPrefab;
    public static string stratname = "Gas";
    public static string stratagemName { get => stratname; set => stratname = value; }

    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemControls> { StratagemControls.Right, StratagemControls.Right, StratagemControls.Down, StratagemControls.Right };
    }

    bool summoned = false;

    private void SpawnBomb()
    {
        Instantiate(EMSPrefab, transform.position + Vector3.up * 100, Quaternion.identity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if (!summoned)
            {
                summoned = true;
                SpawnBomb();
                Invoke("SelfDestroy", 5);                
            }                            
        }
    }

    private void SelfDestroy()
    {
        Destroy(this.gameObject);
    }
}
