using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static StratagemController;

public class AirBurst : MonoBehaviour, IStratagem
{
    public GameObject projectilePrefab { get => EMSPrefab; set => EMSPrefab = value; }

    public GameObject EMSPrefab;
    public static string stratname = "Air burst";
    private int bulletCount = 30;
    private int wavesCount = 5;
    private float radius = 20;
    bool summoned = false;
    public static string stratagemName { get => stratname; set => stratname = value; }

    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemControls> { StratagemControls.Right, StratagemControls.Right, StratagemControls.Right };
    }

    private IEnumerator SpawnBarrage()
    {
        yield return new WaitForSeconds(5);
        Vector3 pos = transform.position + Vector3.up * 170;
        for (int j = 0; j < wavesCount; j++)
        {
            for (int i = 0; i < bulletCount; i++)
            {
                Vector3 bulletPos = pos;
                float xComp = Mathf.Sin(Random.value * Mathf.PI * 2) * radius;                
                float zComp = Mathf.Cos(Random.value * Mathf.PI * 2) * radius;
                bulletPos.x += xComp;                
                bulletPos.z += zComp;                
                Instantiate(EMSPrefab, bulletPos, Quaternion.identity);
            }
            yield return new WaitForSeconds(4f);
        }
        Invoke("SelfDestroy", 5f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if (!summoned)
            {
                StartCoroutine(SpawnBarrage());
                summoned = true;
            }
        }
    }

    private void SelfDestroy()
    {
        Destroy(this.gameObject);
    }
}
