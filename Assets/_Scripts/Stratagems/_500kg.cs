using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static StratagemController;

public class _500kg : MonoBehaviour, IStratagem
{
    [SerializeField]
    private GameObject bombPrefab;

    public GameObject projectilePrefab { get => bombPrefab; set => bombPrefab = value; }    

    public static string stratname = "500kg";
    public static string stratagemName { get => stratname; set => stratname = value; }

    bool summoned = false;

    private void SpawnBomb()
    {
        Instantiate(bombPrefab, transform.position + Vector3.up * 100, Quaternion.identity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if (!summoned)
            {
                summoned = true;
                SpawnBomb();
                Invoke("SelfDestroy", 5);                
            }                            
        }
    }

    private void SelfDestroy()
    {
        Destroy(this.gameObject);
    }

    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemControls> {StratagemControls.Up, StratagemControls.Right, StratagemControls.Down, StratagemControls.Down, StratagemControls.Down };
    }
}
