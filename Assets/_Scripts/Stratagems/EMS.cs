using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static StratagemController;

public class EMS : MonoBehaviour, IStratagem
{
    public GameObject projectilePrefab { get => EMSPrefab; set => EMSPrefab = value; }
    private GameObject EMSPrefab;
    public static string stratname = "EMS";
    public static string stratagemName { get => stratname; set => stratname = value; }
    bool summoned = false;

    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemControls> { StratagemControls.Right, StratagemControls.Right, StratagemControls.Left, StratagemControls.Down };
    }

    private void SpawnBomb()
    {
        Instantiate(EMSPrefab, transform.position + Vector3.up * 100, Quaternion.identity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if (!summoned)
            {
                SpawnBomb();
                Invoke("SelfDestroy", 5);
                summoned = true;
            }
        }
    }

    private void SelfDestroy()
    {
        Destroy(this.gameObject);
    }
}
