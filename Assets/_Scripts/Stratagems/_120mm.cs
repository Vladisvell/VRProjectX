using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static StratagemController;

public class _120mm : MonoBehaviour, IStratagem
{
    public GameObject projectilePrefab { get => EMSPrefab; set => EMSPrefab = value; }

    [SerializeField]
    private GameObject EMSPrefab;
    public static string stratname = "120mm Barrage";
    public static string stratagemName { get => stratname; set => stratname = value; }
    int bombCount = 20;    
    float radius = 20;
    float bombDelay = 2;
    bool spawned = false;

    public static List<StratagemController.StratagemControls> GetSummonSequence()
    {
        return new List<StratagemControls> { StratagemControls.Right, StratagemControls.Right, StratagemControls.Down, StratagemControls.Left, StratagemControls.Right, StratagemControls.Down };
    }

    private IEnumerator SpawnBarrage()
    {
        yield return new WaitForSeconds(5);
        Vector3 pos = transform.position + Vector3.up * 170;
        for (int i = 0; i < bombCount; i++)
        {
            Vector3 bulletPos = pos;
            float xComp = Mathf.Sin(Random.value * Mathf.PI * 2) * radius;
            float yComp = i;
            float zComp = Mathf.Cos(Random.value * Mathf.PI * 2) * radius;
            bulletPos.x += xComp;
            bulletPos.y += yComp;
            bulletPos.z += zComp;
            yield return new WaitForSeconds(bombDelay);
            Instantiate(EMSPrefab, bulletPos, Quaternion.identity);            
        }
        Invoke("DestroyMe", 5);
    }

    public void DestroyMe()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if (spawned)
                return;            
            StartCoroutine(SpawnBarrage());
            spawned = true;
        }
    }
}
