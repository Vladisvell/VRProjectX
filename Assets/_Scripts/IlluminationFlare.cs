using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IlluminationFlare : MonoBehaviour
{
    [SerializeField] float SecondsToFullBrightness;
    [SerializeField] float SecondsToNoBrightness;
    [SerializeField] Light mylight;
    float framesToFullBrightness;
    float framesToNoBrightness;

    float stepBrightUp;
    float stepBrightDown;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroySelf", SecondsToFullBrightness + SecondsToNoBrightness);
        framesToFullBrightness = SecondsToFullBrightness / Time.fixedDeltaTime;
        framesToNoBrightness = SecondsToNoBrightness / Time.fixedDeltaTime;        
        stepBrightUp = mylight.intensity / framesToFullBrightness;
        stepBrightDown = mylight.intensity / framesToNoBrightness;
        mylight.intensity = 0;
    }

    public void DestroySelf()
    {
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(framesToFullBrightness > 0)
        {
            framesToFullBrightness--;
            mylight.intensity += stepBrightUp;
        }
        else
        {
            framesToNoBrightness--;
            mylight.intensity -= stepBrightDown;
        }        
    }
}
