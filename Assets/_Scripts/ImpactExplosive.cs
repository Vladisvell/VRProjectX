using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;

public class ImpactExplosive : MonoBehaviour
{
    public GameObject explosionImpactObject;
    public AudioSource audioSource;
    private GameObject explosion;
    private bool exploded = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if(explosion == null)
            {
                explosion = Instantiate<GameObject>(explosionImpactObject, transform.position, transform.rotation);
                audioSource.Play();
                this.gameObject.GetComponent<Renderer>().enabled = false;
                Invoke("DestroyMe", 3);
            }
            exploded = true;            
        }           
    }

    public void DestroyMe()
    {
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
