using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] float initialVelocity;
    /// <summary>
    /// Spawns a bullet with initial velocity
    /// </summary>
    public void SpawnBullet()
    {
        var obj = Instantiate<GameObject>(bullet, gameObject.transform.position, Quaternion.identity);
        obj.GetComponent<Rigidbody>().position = gameObject.transform.position;
        //obj.gameObject.transform.rotation = gameObject.transform.rotation;
        obj.GetComponent<Rigidbody>().AddRelativeForce(gameObject.transform.forward * initialVelocity, ForceMode.Impulse);
    }
}
