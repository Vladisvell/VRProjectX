using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomChanger : MonoBehaviour
{
    [SerializeField] Camera Camera;
    [SerializeField] float Step = 5.0f;
    float fov = 2f;

    public void IncreaseFov()
    {
        if(Camera.fieldOfView <= 90f - Step)
        {
            Camera.fieldOfView += Step;
        }        
    }

    public void DecreaseFov()
    {
        if(Camera.fieldOfView >= 0f + Step)
        {
            Camera.fieldOfView -= Step;
        }
    }

    public void CycleFov()
    {
        fov += Step;
        fov %= 90f;
        Camera.fieldOfView = Mathf.Clamp(fov, 0f, 90f);
    }
}
