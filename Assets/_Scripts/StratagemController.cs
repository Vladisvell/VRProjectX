﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;

public class StratagemController : MonoBehaviour
{
    public enum StratagemControls
    {
        Up,
        Down,
        Left,
        Right
    }

    public static StratagemController instance;

    public List<IStratagem> KnownStratagems = new List<IStratagem>();

    public Dictionary<List<StratagemControls>, Type> Stratagems = new();
    
    public Dictionary<Type, GameObject> ProjectilePrefabsForStratagems = new();

    [SerializeField]
    private GameObject _500kgPrefab;
    [SerializeField]
    private GameObject _gatlingPrefab;
    [SerializeField]
    private GameObject _empPrefab;
    [SerializeField]
    private GameObject _illuminationPrefab;
    [SerializeField]
    private GameObject _120mmPrefab;
    [SerializeField]
    private GameObject _gasStrikePrefab;
    [SerializeField]
    private GameObject _smokeStrike;
    [SerializeField]
    private GameObject _airBurstStrike;
    

    List<StratagemControls> PushedButtons = new List<StratagemControls>();
        
    [SerializeField] TextMeshProUGUI stratagemStateText;
    [SerializeField] TextMeshProUGUI stratagemTutorialText;
    
    GameObject attachedStratagemBeacon;

    private void Awake()
    {        
        instance = this;        
        GenerateStratagems();
        ProjectilePrefabsForStratagems.Add(typeof(_500kg), _500kgPrefab);
        ProjectilePrefabsForStratagems.Add(typeof(Gatling), _gatlingPrefab);
        ProjectilePrefabsForStratagems.Add(typeof(EMS), _empPrefab);
        ProjectilePrefabsForStratagems.Add(typeof(SmokeStrike), _smokeStrike);
        ProjectilePrefabsForStratagems.Add(typeof(Illumination), _illuminationPrefab);
        ProjectilePrefabsForStratagems.Add(typeof(GasStrike), _gasStrikePrefab);
        ProjectilePrefabsForStratagems.Add(typeof(AirBurst), _airBurstStrike);
        ProjectilePrefabsForStratagems.Add(typeof(_120mm), _120mmPrefab);

        stratagemTutorialText.text = GenerateTutorialText();        
    }    

    private void GenerateStratagems()
    {
        foreach(var i in AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()).Where(type => typeof(IStratagem).IsAssignableFrom(type)))
        {
            if(i.IsInterface)
                continue;
            var method = i.GetMethod("GetSummonSequence", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
            var res = method.Invoke(null, null) as List<StratagemControls>;
            Stratagems.Add(res, i);
        }
        return;
    }

    private string GenerateTutorialText()
    {     
        StringBuilder b = new StringBuilder();
        foreach(var i in Stratagems)
        {
            if(i.Value.GetInterface("IStratagem") != null)
            {
                var prop = i.Value.GetProperty("stratagemName", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.SetProperty | System.Reflection.BindingFlags.GetProperty);
                var stratagemName = prop.GetValue(null);
                b.Append(stratagemName + ": ");
                b.Append(GenerateStratagemString(i.Key) + "\n");
            }
        }
        return b.ToString();
    }

    public void ApplyStratagem()
    {
        if(attachedStratagemBeacon == null)
            return;
        var availableStratagem = Stratagems.Keys.Where(k => Enumerable.SequenceEqual(k, PushedButtons)).FirstOrDefault();
        if(availableStratagem == null)
            return;
        if(attachedStratagemBeacon.GetComponent<IStratagem>() != null)
            return;
        var component = attachedStratagemBeacon.AddComponent(Stratagems[availableStratagem]) as IStratagem;
        component.projectilePrefab = ProjectilePrefabsForStratagems[Stratagems[availableStratagem]];
        ResetStratagemControl();
    }

    public void OnStratagemGrab(GameObject stratagem)
    {
        attachedStratagemBeacon = stratagem;
    }

    public void OnStratagemLoose()
    {
        attachedStratagemBeacon = null;
    }

    public void StratagemControlPushed(StratagemControls control)
    {
        PushedButtons.Add(control);
        if (PushedButtons.Count > 10)
        {
            ResetStratagemControl();
        }
        stratagemStateText.text = GenerateStratagemString(PushedButtons);
    }

    public void ResetStratagemControl()
    {
        PushedButtons.Clear();
        stratagemStateText.text = GenerateStratagemString(PushedButtons);
    }

    private string GenerateStratagemString(List<StratagemControls> controls)
    {
        StringBuilder b = new StringBuilder();
        foreach (StratagemControls control in controls)
        {
            switch (control)
            {
                case StratagemControls.Left:
                    b.Append("←"); break;
                case StratagemControls.Right:
                    b.Append("→"); break;
                case StratagemControls.Up:
                    b.Append("↑"); break;
                case StratagemControls.Down:
                    b.Append("↓"); break;
            }
            //↑→↓←
        }
        return b.ToString();
    }

    public void StratagemUpPushed()
    {
        StratagemControlPushed(StratagemControls.Up);
    }

    public void StratagemDownPushed()
    {
        StratagemControlPushed(StratagemControls.Down);
    }

    public void StratagemLeftPushed()
    {
        StratagemControlPushed(StratagemControls.Left);
    }

    public void StratagemRightPushed()
    {
        StratagemControlPushed(StratagemControls.Right);
    }
}
