using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Interactables;
using UnityEngine.XR.Interaction.Toolkit.Locomotion.Teleportation;
using static Unity.Burst.Intrinsics.X86.Avx;

public class StratagemBeacon : MonoBehaviour
{
    private Vector3 InitialPosition;
    public GameObject StratagemPrefab;
    public GameObject StratagemBeam;
    bool respawned = false;

    // Start is called before the first frame update
    void Start()
    {
        InitialPosition = transform.position;
    }

    public void Respawn()
    {
        if (respawned)
            return;
        var obj = Instantiate<GameObject>(this.gameObject, InitialPosition, Quaternion.identity);
        var comp = obj.GetComponent<Rigidbody>();
        comp.useGravity = true;
        comp.isKinematic = false;
        respawned = true;
    }

    public void EnableBeam()
    {
        StratagemBeam.SetActive(true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<TeleportationArea>() != null)
        {
            if (this.gameObject.GetComponent<IStratagem>() != null)
            {
                var _rb = this.gameObject.GetComponent<Rigidbody>();
                _rb.rotation = Quaternion.identity;
                _rb.freezeRotation = true;                
                this.gameObject.GetComponent<XRGrabInteractable>().enabled = false;
                EnableBeam();
            }
        }

    }

    public void OnGrabbed()
    {
        StratagemController.instance.OnStratagemGrab(this.gameObject);
        var comp = GetComponent<Rigidbody>();
    }

    public void OnReleased()
    {
        StratagemController.instance.OnStratagemLoose();
    }

    private void OnDestroy()
    {

    }
}
